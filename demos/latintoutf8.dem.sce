//
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//


function demo_latintoutf()

  disp("");
  disp("demo latin to utf-8 encoding:");
  
  utf8 = ascii([195 160]);
  disp("ascii([195 160]) is """ + utf8 + """ as utf-8 encoding.");
  
  latin = ascii(224);
  disp("ascii(224) is """ + latin + """ as latin encoding.");

  str = msprintf("%s%s%s", "ascii(latintoutf8(ascii(224))) returns ", ..
                           "[" + strcat(string(ascii(latintoutf8(latin))), " ") + "]", ..
                           " as utf-8 encoding.");
  disp(str);
  
  editor(fullpath(get_absolute_file_path("latintoutf8.dem.sce") + "/latintoutf8.dem.sce"), "readonly");
  
endfunction

demo_latintoutf();
clear demo_latintoutf;

